# Plot alcohol

This plots my alcohol usage from a MySql database.

The plots are created using Plotly: https://plot.ly/python/

<p align="center"><img src="https://gitlab.com/raginggoblin/plotalcohol/-/raw/53c434dd5b5a9e9cc815a382fe8a7160a0d91898/etc/Screenshot_20211227_210422.png" /></p>
