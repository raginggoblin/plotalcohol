package nl.raging.goblin.plotalcohol.persistence

import nl.raging.goblin.plotalcohol.model.DailyIntake
import org.springframework.data.repository.CrudRepository
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDate

@Transactional
interface DailyIntakeRepository : CrudRepository<DailyIntake, Long> {
    fun findAllByOrderByDateAsc(): List<DailyIntake>
    fun findByDateAfterOrderByDateAsc(date: LocalDate): List<DailyIntake>
    fun findByDate(date: LocalDate): DailyIntake?
}
