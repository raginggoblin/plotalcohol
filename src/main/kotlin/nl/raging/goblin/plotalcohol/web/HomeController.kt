package nl.raging.goblin.plotalcohol.web

import nl.raging.goblin.plotalcohol.model.DailyIntake
import nl.raging.goblin.plotalcohol.persistence.DailyIntakeRepository
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import java.time.LocalDate

@Controller
class HomeController(private val dailyIntakeRepository: DailyIntakeRepository) {

    @GetMapping("/")
    fun plotAlcoholUsage(model: Model): String {
        val halfAYearAgo = LocalDate.now().minusMonths(6)
        val sparseDailyIntakes: List<DailyIntake> = dailyIntakeRepository.findByDateAfterOrderByDateAsc(halfAYearAgo)
        val consecutiveAlcoholDays = consecutiveAlcoholDays(sparseDailyIntakes)
        model.addAttribute("epochDays", consecutiveAlcoholDays.map { it.date.toEpochDay() })
        model.addAttribute("usagePerDay", consecutiveAlcoholDays.map { it.usage() })
        model.addAttribute("movingAverage7", movingAverage(consecutiveAlcoholDays, 7))
        model.addAttribute("movingAverage30", movingAverage(consecutiveAlcoholDays, 30))
        model.addAttribute("movingAverage100", movingAverage(consecutiveAlcoholDays, 100))
        model.addAttribute("minDate", consecutiveAlcoholDays.last().date.minusMonths(6).toString())
        model.addAttribute("maxDate", consecutiveAlcoholDays.last().date.toString())
        return "home"
    }

    private fun movingAverage(dailyIntakes: List<DailyIntake>, period: Int): MutableList<Double> {
        val movingAverage = mutableListOf<Double>()
        val usage = dailyIntakes.map { it.usage() }

        for (i in usage.indices) {
            val start = if (i < period) 0 else i - period + 1
            val end = i + 1
            val average = usage.subList(start, end).average()
            movingAverage.add(i, average)
        }

        return movingAverage
    }
}
