package nl.raging.goblin.plotalcohol.web

import nl.raging.goblin.plotalcohol.model.DailyIntake
import java.time.LocalDate

fun consecutiveAlcoholDays(sparseDailyIntakes: List<DailyIntake>): MutableList<DailyIntake> {
    val result = mutableListOf<DailyIntake>()

    if (sparseDailyIntakes.isNotEmpty()) {
        val today = LocalDate.now()
        var current = sparseDailyIntakes.get(0).date

        while (!current.isAfter(today)) {
            val currentAlcoholDay = sparseDailyIntakes.firstOrNull { it.date.equals(current) }
            if (currentAlcoholDay != null) {
                result.add(currentAlcoholDay)
            } else {
                result.add(DailyIntake(date = current))
            }
            current = current.plusDays(1)
        }
    } else {
        result.add(DailyIntake())
    }

    return result;
}

