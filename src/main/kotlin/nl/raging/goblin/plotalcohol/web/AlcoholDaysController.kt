package nl.raging.goblin.plotalcohol.web

import nl.raging.goblin.plotalcohol.model.DailyIntake
import nl.raging.goblin.plotalcohol.persistence.DailyIntakeRepository
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate


@Controller
class AlcoholDaysController(private val dailyIntakeRepository: DailyIntakeRepository) {

    @GetMapping("/alcoholdays")
    fun plotAlcoholUsage(model: Model): String {
        val sparseDailyIntakes: List<DailyIntake> = dailyIntakeRepository.findAllByOrderByDateAsc()
        val consecutiveAlcoholDays = consecutiveAlcoholDays(sparseDailyIntakes)
        model.addAttribute("epochDays", consecutiveAlcoholDays.map { it.date.toEpochDay() })
        model.addAttribute("minDate", LocalDate.now().minusYears(10).toString())
        model.addAttribute("maxDate", LocalDate.now().toString())
        model.addAttribute("radler", consecutiveAlcoholDays.map { it.radler })
        model.addAttribute("pils", consecutiveAlcoholDays.map { it.pils })
        model.addAttribute("wijn", consecutiveAlcoholDays.map { it.wijn })
        model.addAttribute("speciaalBier", consecutiveAlcoholDays.map { it.speciaalBier })
        model.addAttribute("borrel", consecutiveAlcoholDays.map { it.borrel })
        return "alcoholdays"
    }
}

@RestController
class AlcoholDaysRestController(private val dailyIntakeRepository: DailyIntakeRepository) {

    @PostMapping("/alcoholdays")
    fun parseAlcoholUsageForm(
        @RequestParam("date") date: String,
        @RequestParam("radler") radler: Int,
        @RequestParam("pils") pils: Int,
        @RequestParam("wijn") wijn: Int,
        @RequestParam("speciaal-bier") speciaalBier: Int,
        @RequestParam("borrel") borrel: Int
    ) {

        val localDate = LocalDate.parse(date)
        var dailyIntake: DailyIntake? = dailyIntakeRepository.findByDate(localDate)
        if (dailyIntake == null) {
            dailyIntake = DailyIntake(
                date = localDate,
                radler = radler,
                pils = pils,
                wijn = wijn,
                speciaalBier = speciaalBier,
                borrel = borrel
            )
        } else {
            dailyIntake.radler = radler
            dailyIntake.pils = pils
            dailyIntake.wijn = wijn
            dailyIntake.speciaalBier = speciaalBier
            dailyIntake.borrel = borrel
        }
        dailyIntakeRepository.save(dailyIntake)
    }
}
