package nl.raging.goblin.plotalcohol

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PlotalcoholApplication

fun main(args: Array<String>) {
    runApplication<PlotalcoholApplication>(*args)
}
