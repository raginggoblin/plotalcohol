package nl.raging.goblin.plotalcohol.model

import jakarta.persistence.*
import jakarta.persistence.GenerationType.IDENTITY
import java.time.LocalDate

@Entity
class DailyIntake(

    @Id
    @GeneratedValue(strategy = IDENTITY)
    var id: Long = 0,
    var date: LocalDate = LocalDate.now(),
    var radler: Int = 0,
    var pils: Int = 0,
    var wijn: Int = 0,
    var speciaalBier: Int = 0,
    var borrel: Int = 0,

    ) {

    fun usage(): Double {
        return radler * 0.5 + pils + wijn + speciaalBier * 1.5 + borrel
    }

    override fun toString(): String {
        return "AlcoholDay(id=$id, date=$date, radler=$radler, pils=$pils, wijn=$wijn, speciaalBier=$speciaalBier, borrel=$borrel)"
    }
}
